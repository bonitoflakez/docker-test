import express from 'express';
import cors from 'cors';

const app = express();

app.use(cors());
app.use(express.json());

app.get('/hello', (req, res, next) => {
    return res.status(200).json({
        message: "Hi from docker"
    })
})

app.listen(3000, () => {
    console.log("server is up and running")
})